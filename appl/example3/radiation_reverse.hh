// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Class for calculating radiation at the interface of a pm - ff model
 */

#ifndef DUMUX_RADIATION_EQUILIBRIUM_REVERSE_HH
#define DUMUX_RADIATION_EQUILIBRIUM_REVERSE_HH

namespace Dumux {

/*!
 * \brief Class for calculating radiation at the interface of a pm - ff model
 */
template <class Scalar, class FVGridGeometry>
class RadiationReverse
{
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using Element = typename GridView::template Codim<0>::Entity;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

public:
    template<class VolumeVariables>
    static Scalar radationEquilibrium(const Element& element,
                                      const VolumeVariables &volVars,
                                      const SubControlVolumeFace& scvf,
                                      const Scalar ffTemp,
                                      const Scalar time)
    {
        using FluidSystem = typename VolumeVariables::FluidSystem;

        //get parameters of interface
        double amplitude = getParam<double>("Grid.Amplitude");
        double offset = getParam<double>("Grid.Offset");
        double scaling = getParam<double>("Grid.Scaling");

        //calculate slope with chain rule (sin x = cos x * derivative of x)
        using std::cos;
        Scalar slopeRidge = cos((element.geometry().center()[0] -offset) / scaling * 2.0 * M_PI)*M_PI*2/scaling*amplitude;

        //to get the angle from the slope use the arc tangent
        using std::atan;
        Scalar angleIrradiance = atan(slopeRidge);

        Scalar solarIrradiance = 0.0;
        Scalar surfaceAlbedo = 0.0;
        Scalar boltzmannConstant = 5.67e-8; //W/m^2K^4
        Scalar surfaceEmissivity = 0.0;
        Scalar atmosphericEmissivity = 0.0;
        Scalar surfaceTemp = 0.0;
        Scalar waterContent = volVars.saturation(FluidSystem::liquidPhaseIdx)*volVars.porosity();
        Scalar timeInHours = time/3600 + 6; //time in hours

        surfaceTemp +=  volVars.temperature();

        using std::pow;
        using std::pow;
        Scalar exponent =  1.0/7.0;
        //vaporPressure in hPa
        atmosphericEmissivity += 1.24*pow((FluidSystem::H2O::vaporPressure(ffTemp)/100/ffTemp),(exponent));

        if (waterContent < 0.3)
            surfaceEmissivity += 0.93 + 0.1333*waterContent;
        else
            surfaceEmissivity += 0.97;

        if (waterContent > 0.3)
            surfaceAlbedo += 0.075;
        else if (waterContent < 0.04)
            surfaceAlbedo += 0.17;
        else
            surfaceAlbedo += 0.1846 - 0.3654*waterContent;

        //calculate surface irradiance depending on the slope of the area. angleIrradiance has to be substracted in case sun goes up on the right and sets on the left. also calculation of shadow then needs to be switched
        Scalar s = 800*cos(2*M_PI*((timeInHours + 12)/24) - angleIrradiance);

        //if solarIrradiance on a flat surface (hence no addition of angle irradiance) is smaller than 0 it means it is night
        if (cos(2*M_PI*((timeInHours + 12)/24)) < 0.0)
            solarIrradiance = 0.0;
        else
            solarIrradiance += s;

            //calculate shadowlength. If in shadow, irradiance is 0. Function is positive for angles fo incidence < 90° (because we substract the angle of incidence from 90 degrees) and positive for higher angles of indicence. For angles less than 90 degrees the shadow is right of the trapeze (sun from left) for higher angles left of trapeze.
            using std::tan;
            Scalar angleIncidence = (2*M_PI*(timeInHours+12)/24) - (1.5*M_PI);
            Scalar shadowlength =std::tan((M_PI/2)-angleIncidence)*2*amplitude;
            Scalar numberOfHills = 1/scaling;
            for (int topIdx = 0.0; topIdx <numberOfHills; topIdx++)
            {
                const auto topPosition = 0.25*scaling+offset + topIdx*scaling;

                if (shadowlength > 0.0)
                {
                    if(element.geometry().center()[0] > topPosition-shadowlength && element.geometry().center()[0] < topPosition)
                        solarIrradiance = 0.0;
                }
                else if (shadowlength < 0.0)
                {
                    if(element.geometry().center()[0] > topPosition && element.geometry().center()[0] < topPosition+shadowlength)
                        solarIrradiance = 0.0;

                }
            }

        Scalar radiation = solarIrradiance*(1-surfaceAlbedo)+boltzmannConstant*surfaceEmissivity*(atmosphericEmissivity*pow(ffTemp,4) - pow(surfaceTemp,4));

        //radiation in W/m^2 = J/(s*m^2)
        return radiation;
    }


};

} // end namespace Dumux

#endif
