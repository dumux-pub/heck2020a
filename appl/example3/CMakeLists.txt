add_input_file_links()

dune_add_test(NAME test_stokes1p2cdarcy2p2c_radiation_wavy
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMPILE_DEFINITIONS RADIATION=Radiation)

dune_add_test(NAME test_stokes1p2cdarcy2p2c_radiation_wavy_reverse
              SOURCES main.cc
              CMAKE_GUARD HAVE_UMFPACK
              COMPILE_DEFINITIONS RADIATION=RadiationReverse)
