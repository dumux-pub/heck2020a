// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief This file can be used to read input like experimental data from a
 *        file. The evaluate function can be used to obtain the right values
 *        and setting them as boundary conditions.
 */
#ifndef DUMUX_READ_DATA_FILE_HH
#define DUMUX_READ_DATA_FILE_HH

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <limits>
#include <cmath>

#include <dune/common/debugstream.hh>

namespace Dumux
{

void printData(std::vector<double> data[2])
{
  for (unsigned int i = 0; i < data[0].size(); ++i)
      std::cout << data[0][i] << " " << data[1][i]  << std::endl;
}

void checkData(std::vector<double> data[2], std::string dataName="")
{
  double min[2] = {std::numeric_limits<double>::max(), std::numeric_limits<double>::max()};
  double max[2] = {std::numeric_limits<double>::min(), std::numeric_limits<double>::min()};
  double sum = 0.0;
  for (unsigned int i = 0; i < data[0].size(); ++i)
  {
    if (data[0][i] > data[0][i+1] && i+1 < data[0].size())
    {
      std::cout << "Error: time data is not increasing between number " << i+1 << " and " << i+2 << "." << std::endl;
      DUNE_THROW(Dune::IOError, "Error, see line before.");
    }

    if (std::isnan(data[1][i]))
    {
      std::cout << "Error: data point " << i+1 << " is not a number." << std::endl;
      DUNE_THROW(Dune::IOError, "Error, see line before.");
    }

    if (std::isinf(data[1][i]))
    {
      std::cout << "Error: data point " << i+1 << " is not a inf." << std::endl;
      DUNE_THROW(Dune::IOError, "Error, see line before.");
    }

    if (data[1][i] < 1e-10 && data[1][i] > -1e-10)
      std::cout << "Warning: data point " << i+1 << " is zero." << std::endl;

    if (data[0][i] > max[0])
      max[0] = data[0][i];

    if (data[0][i] < min[0])
      min[0] = data[0][i];

    if (data[1][i] > max[1])
      max[1] = data[1][i];

    if (data[1][i] < min[1])
      min[1] = data[1][i];

    sum += data[1][i];
  }
  std::cout << dataName
            << " min[0]: " << min[0] << " max[0]: " << max[0]
            << " min[1]: " << min[1] << " max[1]: " << max[1]
            << " mean[1]: " << sum/data[1].size() << " numEntries[1]: " << data[1].size()
            << std::endl;
}

int readData(std::string filename, std::vector<double> data[2])
{
  std::string line;
  std::ifstream myfile (filename.c_str());
  if (myfile.is_open())
  {
    unsigned int i = 0;
    while (std::getline (myfile, line)) // first line is a comment
    {
      double a, b;
      i++;
      if (myfile >> a >> b)
      {
        data[0].push_back(a);
        data[1].push_back(b);
      }
      else
        std::cout << "Warning: could not pass value number: " << i << std::endl;
    }
    myfile.close();
    checkData(data, filename);
    return 1;
  }
  else
  {
    std::cout << "Unable to open file: " << filename << "." << std::endl;
    DUNE_THROW(Dune::IOError, "Error, see line before.");
  }

  return 0;
}

const double evaluateData(const std::vector<double> data[2],
                          const double startTime,
                          const double endTime,
                          const unsigned int averagingMethod = 3)
{
  if(startTime > endTime)
    DUNE_THROW(Dune::RangeError, "startTime is greater than endTime.");

  int startIdx = -1;
  int endIdx = -1;
  for (unsigned int i = 0; i < data[0].size(); ++i)
  {
    if (data[0][i] > startTime && startIdx == -1)
      startIdx = i;
    if (data[0][i] > endTime && endIdx == -1)
      endIdx = i-1;
  }

  if (startIdx == -1)
    startIdx = data[0].size()-1;
  if (endIdx == -1)
    endIdx = data[0].size()-1;

//   std::cout << "startIdx: " << startIdx << " endIdx: " << endIdx << "  :  " << data[0][startIdx] << " " << data[0][endIdx] << std::endl;
  // exactly one data point in interval
  if (startIdx == endIdx)
    return data[1][startIdx];
  // no value in interval
  if (startIdx > endIdx) //! \todo use interpolation here
    return data[1][startIdx-1];
  // time is large the input data
  if (startIdx == -1 && endIdx == -1)
    return data[1][data[1].size()-1];
  // more than one data point in interval
  if (startIdx < endIdx)
  {
    double sum = 0.0;

    // first value
    if (averagingMethod == 0)
      return data[1][startIdx];

    // last value
    if (averagingMethod == 1)
      return data[1][endIdx];

    // arithmetic averaging
    if (averagingMethod == 2)
    {
      for (unsigned int i = startIdx; i < endIdx+1; ++i)
      {
        sum += data[1][i];
      }
      return sum/(endIdx - startIdx + 1);
    }

    // data (e.g time) weighted averaging
    if (averagingMethod == 3)
    for (unsigned int i = startIdx; i < endIdx+1; ++i)
    {
      double deltaT = 0.0;
      if (i == startIdx)
//         {
        deltaT = (data[0][i+1] + data[0][i]) / 2.0 - startTime;
//       std::cout << data[0][i+1] << " "<< data[0][i] << " " << startTime << std::endl;
//         }
      else if (i == endIdx)
        deltaT = endTime - (data[0][i] + data[0][i-1]) / 2.0;
      else
        deltaT = (data[0][i+1] + data[0][i]) / 2.0 - (data[0][i] + data[0][i-1]) / 2.0;
//       std::cout << data[0][i] << " "<< data[0][i+1] << " " << deltaT  << " "<<  data[1][i] << std::endl;
      sum += data[1][i] * deltaT;
    }
    return sum/(endTime-startTime);
  }

  DUNE_THROW(Dune::Exception, "Unknown evaluation error!");
  return(0);
}

void writeDataFile(std::vector<double> data[2],
                   std::string dataName[2],
                   std::string filename)
{
  std::ofstream myfile (filename.c_str());
  if (myfile.is_open())
  {
    myfile << "#" << dataName[0] << " " << dataName[1] << std::endl;
    for (unsigned int i = 0; i < data[0].size(); ++i)
      myfile << data[0][i] << " " << data[1][i] << std::endl;

    myfile.close();
    checkData(data, filename);
  }
  else
  {
    std::cout << "Unable to open file: " << filename << "." << std::endl;
    DUNE_THROW(Dune::IOError, "Error, see line before.");
  }
}
















/*!
 * \brief Checks and prints data.
 */
template<typename T>
void checkDataNew(const std::vector<std::vector<T>> data)
{
    for (unsigned int i = 0; i < data.size(); ++i)
    {
        for (unsigned int j = 0; j < data[i].size(); ++j)
        {
            std::cout << "data[" << i << "][" << j << "]: " << data[i][j] << std::endl;
        }
        std::cout << std::endl;
    }
}

/*!
 * \brief Writes data to a file with custom delimiters.
 */
template<typename T>
void writeDataFileNew(std::string filename, const std::vector<std::vector<T>> data,
                      char delimiter = ',', const std::vector<std::string> datanames = {""})
{
    std::ofstream outfile (filename.c_str());
    if (outfile.is_open())
    {
        // header
        outfile << "#";
        for (unsigned int i = 0; i < datanames.size(); ++i)
        {
            outfile << datanames[i];
            if (i < datanames.size()-1)
                outfile << delimiter;
        }
        outfile << std::endl;

        // data
        for (unsigned int i = 0; i < data.size(); ++i)
        {
            for (unsigned int j = 0; j < data[i].size(); ++j)
            {
                outfile << data[i][j];
                if (j < data[i].size()-1)
                    outfile << delimiter;
            }
            outfile << std::endl;
        }
        outfile << std::endl;

        outfile.close();
    }
    else
    {
        std::cout << "Could not open file: " << filename << "." << std::endl;
    }
}

/*!
 * \brief Reads data from arbitrary file with arbitrary delimiters.
 */
template<typename T>
void readDataNew(std::string filename, std::vector<std::vector<T>> &data,
                 char delimiter = ',')
{
    std::vector<std::vector<std::string>> input;
    std::ifstream infile(filename);

    while (infile)
    {
        std::string string1;
        if (!getline(infile, string1))
            break;

        std::istringstream stringStream(string1);
        std::vector<std::string> record;

        bool isComment = false;
        while (stringStream)
        {
            std::string string2;

            if (!getline(stringStream, string2, delimiter))
                break;
            if (string2[0] == '#')
                isComment = true;

            record.push_back(string2);
        }

        if (!isComment)
            input.push_back(record);
    }

    for (unsigned int i = 0; i < input.size(); ++i)
    {
        std::vector<T> temp;
        for (unsigned int j = 0; j < input[i].size(); ++j)
        {
            try
            {
                temp.push_back((T)stof(input[i][j]));
            }
            catch (...)
            {
                std::cout << "Could not read line from file: " << filename << std::endl;
                std::cout << "Data point: " << input[i][j] << std::endl;
                exit(1);
            }
        }
        data.push_back(temp);
    }

    if (!infile.eof())
    {
        std::cout << "Could not open file: " << filename << std::endl;
        exit(1);
    }
}

/*!
 * \brief Returns one data point value which is closet to the given position
 */
template<typename T, int dim>
T getNearestDataPoint(std::vector<std::vector<T>> &data,
                      Dune::FieldVector<double, dim> globalPos,
                      Dune::FieldVector<int, dim> coordinateColumns,
                      int dataColumn,
                      double maxDistanceTreshold = 9e9,
                      std::string fieldName = "emptyField")
{
    T value;
    Dune::FieldVector<double, dim> globalPosFound;
    double distance = 9e99;
    for (unsigned int i = 0; i < data.size(); ++i)
    {
        Dune::FieldVector<T, dim> temp(globalPos);
        Dune::FieldVector<T, dim> globalPosFoundTemp(globalPos);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
            temp[curDim] -= data[i][coordinateColumns[curDim]];
            globalPosFoundTemp[curDim] = data[i][coordinateColumns[curDim]];
        }
        if (temp.two_norm() < distance)
        {
            distance = temp.two_norm();
            value = data[i][dataColumn];
            globalPosFound = globalPosFoundTemp;
        }
    }
    if (distance > maxDistanceTreshold)
        std::cout << "Exceeded maxDistanceTreshold! Distance: " << distance
                  << " for globalPos " << globalPos
                  << ". In field: " << fieldName
                  << " value: " << value
                  << " @" << globalPosFound
                  << std::endl;
    return value;
}

/*!
 * \brief Returns one data point value which has been interpolated from
 *        the four closest available data points
 */
template<typename T, int dim>
T interpolateDataPoint(std::vector<std::vector<T>> &data,
                       Dune::FieldVector<double, dim> globalPos,
                       Dune::FieldVector<int, dim> coordinateColumns,
                       int dataColumn)
{
    // TODO: only works for 2D
    // TODO even for 2D not really tests, results look ok, but
    //      was a segfault issue when saying averageDataPoint[1] = dataSets[3][dataColumn];
    std::vector<std::vector<T>> dataSets;
    dataSets.resize(2*dim);
    std::vector<bool> useDataSet;
    useDataSet.resize(2*dim, false);
    std::vector<double> distance;
    distance.resize(2*dim, 9e9);
    for (unsigned int i = 0; i < data.size(); ++i)
    {
        Dune::FieldVector<T, dim> temp(globalPos);
        temp[0] -= data[i][coordinateColumns[0]];
        temp[1] -= data[i][coordinateColumns[1]];
        double tempDistance = temp.two_norm();

        // closest point from lower left side
        if (globalPos[0] > temp[0] - 1e-8
            && globalPos[1] > temp[1] - 1e-8
            && tempDistance < distance[0])
        {
            distance[0] = tempDistance;
            dataSets[0] = data[i];
            useDataSet[0] = true;
        }
        // closest point from lower right side
        if (globalPos[0] < temp[0] + 1e-8
            && globalPos[1] > temp[1] - 1e-8
            && tempDistance < distance[1])
        {
            distance[1] = tempDistance;
            dataSets[1] = data[i];
            useDataSet[1] = true;
        }
        // closest point from upper left side
        if (globalPos[0] > temp[0] - 1e-8
            && globalPos[1] < temp[1] + 1e-8
            && tempDistance < distance[2])
        {
            distance[2] = tempDistance;
            dataSets[2] = data[i];
            useDataSet[2] = true;
        }
        // closest point from upper right side
        if (globalPos[0] < temp[0] + 1e-8
            && globalPos[1] < temp[1] + 1e-8
            && tempDistance < distance[3])
        {
            distance[3] = tempDistance;
            dataSets[3] = data[i];
            useDataSet[3] = true;
        }

        // is exactly the same point
        if (globalPos[0] < temp[0] + 1e-8
            && globalPos[0] > temp[0] - 1e-8
            && globalPos[1] < temp[1] + 1e-8
            && globalPos[1] > temp[1] - 1e-8)
        {
            return data[i][dataColumn];
        }
    }

    T dataPoint = 0.0;
    T averageDataPoint[2];
    double tempDistance = 0.0;
    double leftDistance = 0.0;
    double rightDistance = 0.0;
    // distance average between lower left and lower right
    if (useDataSet[0] && useDataSet[1])
    {
        tempDistance = dataSets[1][coordinateColumns[0]]
                      - dataSets[0][coordinateColumns[0]];
        leftDistance = globalPos[0]
                      - dataSets[0][coordinateColumns[0]];
        rightDistance = dataSets[1][coordinateColumns[0]]
                      - globalPos[0];
        averageDataPoint[0] = (leftDistance * dataSets[1][dataColumn]
                                + rightDistance * dataSets[0][dataColumn])
                              / tempDistance;
    }
    else if (useDataSet[0])
    {
        averageDataPoint[0] = dataSets[0][dataColumn];
    }
    else // useDataSet[1]
    {
        averageDataPoint[0] = dataSets[1][dataColumn];
        dataSets[0] = dataSets[1];
    }
    // distance average between upper left and upper right
    if (useDataSet[2] && useDataSet[3])
    {
        tempDistance = dataSets[3][coordinateColumns[0]]
                      - dataSets[2][coordinateColumns[0]];
        leftDistance = globalPos[0]
                      - dataSets[2][coordinateColumns[0]];
        rightDistance = dataSets[3][coordinateColumns[0]]
                      - globalPos[0];
        averageDataPoint[1] = (leftDistance * dataSets[1][dataColumn]
                                + rightDistance * dataSets[0][dataColumn])
                              / tempDistance;
    }
    else if (useDataSet[2])
    {
        averageDataPoint[1] = dataSets[2][dataColumn];
    }
    else if (useDataSet[3])
    {
        averageDataPoint[1] = dataSets[3][dataColumn];
        dataSets[2] = dataSets[3];
    }
    // distance average between lower and upper
    if ((useDataSet[0] || useDataSet[1])
        && (useDataSet[2] || useDataSet[3]))
    {
        tempDistance = dataSets[2][coordinateColumns[1]]
                      - dataSets[0][coordinateColumns[1]];
        leftDistance = globalPos[1]
                      - dataSets[0][coordinateColumns[1]];
        rightDistance = dataSets[2][coordinateColumns[1]]
                      - globalPos[1];
        dataPoint = (leftDistance * averageDataPoint[1]
                      + rightDistance * averageDataPoint[0])
                    / tempDistance;
    }
    else if (useDataSet[0] || useDataSet[1])
        dataPoint = averageDataPoint[0];
    else // (useDataSet[2] || useDataSet[3])
        dataPoint = averageDataPoint[1];

//     std::cout << globalPos << ": " << dataPoint << std::endl;
    if (std::isnan(dataPoint))
        dataPoint = 0.0;
    return dataPoint;
}

/*!
 * \brief Returns one data set which is closet to the given position
 */
template<typename T, int dim>
std::vector<T> getNearestDataSet(std::vector<std::vector<T>> &data,
                                 Dune::FieldVector<double, dim> globalPos,
                                 Dune::FieldVector<int, dim> coordinateColumns)
{
    std::vector<T> dataSet;
    double distance = 9e99;
    for (unsigned int i = 0; i < data.size(); ++i)
    {
        Dune::FieldVector<T, dim> temp(globalPos);
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
            temp[curDim] -= data[i][coordinateColumns[curDim]];
        if (temp.two_norm() < distance)
        {
            distance = temp.two_norm();
            dataSet = data[i];
        }
    }
    return dataSet;
}

} // namespace Dumux

#endif // DUMUX_READ_DATA_FILE_HH
