// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief A simple Stokes test problem for the staggered grid (Navier-)Stokes model.
 */
#ifndef DUMUX_STOKES1P2C_RADIATION_SUBPROBLEM_HH
#define DUMUX_STOKES1P2C_RADIATION_SUBPROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include "readwritedatafile.hh"

#include <dumux/material/fluidsystems/1padapter.hh>
#include <dumux/h2oair.hh>

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/compositional/navierstokesncmodel.hh>

#include <dumux/freeflow/compositional/komegancmodel.hh>
#include <dumux/freeflow/rans/twoeq/komega/problem.hh>
#include <dumux/freeflow/turbulenceproperties.hh>
namespace Dumux
{
template <class TypeTag>
class StokesSubProblem;

namespace Properties
{

// Create new type tags
namespace TTag {
struct StokesOnePTwoCTypeTag { using InheritsFrom = std::tuple<KOmegaNCNI, StaggeredFreeFlowModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::StokesOnePTwoCTypeTag> { using type = Dune::YaspGrid<2, Dune::TensorProductCoordinates<GetPropType<TypeTag, Properties::Scalar>, 2> >; };

// The fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::StokesOnePTwoCTypeTag>
{
  using H2OAir = FluidSystems::H2OAir<GetPropType<TypeTag, Properties::Scalar>>;
  static constexpr auto phaseIdx = H2OAir::gasPhaseIdx; // simulate the water phase
  using type = FluidSystems::OnePAdapter<H2OAir, phaseIdx>;
};

template<class TypeTag>
struct ReplaceCompEqIdx<TypeTag, TTag::StokesOnePTwoCTypeTag> { static constexpr int value = 5; };

// Use formulation based on mass fractions
template<class TypeTag>
struct UseMoles<TypeTag, TTag::StokesOnePTwoCTypeTag> { static constexpr bool value = true; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::StokesOnePTwoCTypeTag> { using type = Dumux::StokesSubProblem<TypeTag> ; };

template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::StokesOnePTwoCTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::StokesOnePTwoCTypeTag> { static constexpr bool value = true; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::StokesOnePTwoCTypeTag> { static constexpr bool value = true; };
}

/*!
 * \ingroup NavierStokesTests
 * \brief  Test problem for the one-phase (Navier-) Stokes problem.
 *
 * Horizontal flow from left to right with a parabolic velocity profile.
 */
template <class TypeTag>
class StokesSubProblem : public RANSProblem<TypeTag>
{
    using ParentType = RANSProblem<TypeTag>;

    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;

    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using ElementFaceVariables = typename GetPropType<TypeTag, Properties::GridFaceVariables>::LocalView;
    using FluidState = GetPropType<TypeTag, Properties::FluidState>;

    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;
    using TimeLoopPtr = std::shared_ptr<TimeLoop<Scalar>>;

    using DiffusionCoefficientAveragingType = typename StokesDarcyCouplingOptions::DiffusionCoefficientAveragingType;

    using H2O = Components::TabulatedComponent<Components::H2O<Scalar> >;

    static constexpr bool useMoles = GetPropType<TypeTag, Properties::ModelTraits>::useMoles();
    static constexpr auto dimWorld = FVGridGeometry::GridView::dimensionworld;

public:
    StokesSubProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry, std::shared_ptr<CouplingManager> couplingManager)
    : ParentType(fvGridGeometry, "Stokes"), eps_(1e-6), couplingManager_(couplingManager)
    {

        refPressure_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.RefPressure");
        initializationTime_ = getParamFromGroup<Scalar>(this->paramGroup(), "Problem.InitializationTime");

        Dumux::TurbulenceProperties<Scalar, dimWorld, true> turbulenceProperties;
        FluidState fluidState;
        const auto phaseIdx = 0;
        fluidState.setPressure(phaseIdx, 1e5);
        fluidState.setTemperature(273.15+16);
        fluidState.setMoleFraction(phaseIdx, 1/*FluidSystem::AirIdx*/, 1.0);
        fluidState.setMoleFraction(phaseIdx, 0/*FluidSystem::H2OIdx*/, 0.0);
        Scalar density = FluidSystem::density(fluidState, 0);
        Scalar kinematicViscosity = FluidSystem::viscosity(fluidState, phaseIdx) / density;
        Scalar diameter = this->gridGeometry().bBoxMax()[1] - this->gridGeometry().bBoxMin()[1];

        turbulentKineticEnergy_ = turbulenceProperties.turbulentKineticEnergy(0.3 /*refVelocity*/, diameter, kinematicViscosity);
        dissipation_ = turbulenceProperties.dissipationRate(0.3 /*refVelocity_*/, diameter, kinematicViscosity);

        temperatureDataFilePM_ =getParam<std::string>("Problem.TemperatureDataFile");
        readData(temperatureDataFilePM_, temperatureDataPM_);

        relativeHumidityDataFilePM_ =getParam<std::string>("Problem.RelativeHumidityDataFile");
        readData(relativeHumidityDataFilePM_, relativeHumidityDataPM_);

        //for laminar/turbulent flow
        windSpeedDataFilePM_=getParam<std::string>("Problem.WindSpeedDataFile");
        readData(windSpeedDataFilePM_, windSpeedDataPM_);
    }


    void setTime(Scalar time)
    { time_ = time; }

    const Scalar time() const
    {return time_; }

    void setPreviousTime(Scalar time)
    { previousTime_ = time; }

    const Scalar previousTime() const
    {return previousTime_; }


    template<class SolutionVector, class GridVariables>
    void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar timeStepSize)

    {
        if (time_>=initializationTime_)
        {
        // compute the mass in the entire domain
            Scalar evaporation = 0.0;
            Scalar energyFlux = 0.0;
            // bulk elements
            for (const auto& element : elements(this->gridGeometry().gridView()))
            {
                auto fvGeometry = localView(this->gridGeometry());
                fvGeometry.bindElement(element);

                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bindElement(element, fvGeometry, curSol);

                auto elemFaceVars = localView(gridVariables.curGridFaceVars());
                elemFaceVars.bindElement(element, fvGeometry, curSol);

                for (auto&& scvf : scvfs(fvGeometry))
                {
                    if (!couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf))
                        continue;

                    // NOTE: binding the coupling context is necessary
                    couplingManager_->bindCouplingContext(CouplingManager::stokesIdx, element);
                    const auto flux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);

                    const auto fluxEnergy = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);

                    evaporation += flux[1] * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                    energyFlux += (fluxEnergy) * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                }
            }
            evaporation = evaporation * FluidSystem::molarMass(1);
            std::cout<<"evaporation in ff "<<evaporation<<std::endl;
            std::cout<<"energyFlux in ff "<<energyFlux<<std::endl;
        }
    }

   /*!
     * \name Problem parameters
     */
    // \{

   /*!
     * \brief Return the temperature within the domain in [K].
     */
    Scalar temperature() const
    { return refTemperature_; }

   /*!
     * \brief Return the sources within the domain.
     *
     * \param globalPos The global position
     */
    NumEqVector sourceAtPos(const GlobalPosition &globalPos) const
    { return NumEqVector(0.0); }

    // \}
   /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param element The finite element
     * \param scvf The sub control volume face
     */
    BoundaryTypes boundaryTypes(const Element& element,
                                const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        const auto& globalPos = scvf.center();

        values.setNeumann(Indices::energyEqIdx);

        if (onLeftBoundary_(globalPos))
        {
            values.setDirichlet(Indices::conti0EqIdx + 1);
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            values.setDirichlet(Indices::turbulentKineticEnergyIdx);
            values.setDirichlet(Indices::dissipationIdx);
            values.setDirichlet(Indices::temperatureIdx);
        }

        else if (onRightBoundary_(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
            values.setOutflow(Indices::conti0EqIdx + 1);
            values.setOutflow(Indices::turbulentKineticEnergyEqIdx);
            values.setOutflow(Indices::dissipationEqIdx);
            values.setOutflow(Indices::energyEqIdx);
        }
        else if (couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf))
        {
            values.setCouplingNeumann(Indices::conti0EqIdx);
            values.setCouplingNeumann(Indices::conti0EqIdx + 1);
            values.setCouplingNeumann(Indices::momentumYBalanceIdx);
            values.setBJS(Indices::momentumXBalanceIdx);
            values.setCouplingNeumann(Indices::energyEqIdx);
        }
        else if (onUpperBoundary_(globalPos))
        {
            values.setAllSymmetry();
        }
        else
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            values.setNeumann(Indices::conti0EqIdx);
            values.setNeumann(Indices::conti0EqIdx + 1);
            values.setDirichlet(Indices::turbulentKineticEnergyEqIdx);
            values.setDirichlet(Indices::dissipationEqIdx);
            values.setNeumann(Indices::energyEqIdx);
        }
        return values;
    }

    /*!
      * \brief Evaluate the boundary conditions for fixed values at cell centers
      *
      * \param element The finite element
      * \param scv the sub control volume
      * \note used for cell-centered discretization schemes
      */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolume &scv) const
    {
        const auto globalPos = scv.center();
        PrimaryVariables values(initialAtPos(globalPos));
        using std::pow;
        unsigned int elementIdx = this->gridGeometry().elementMapper().index(element);
        const auto wallDistance = ParentType::wallDistance_[elementIdx];
        values[Indices::dissipationEqIdx] = 6.0 * ParentType::kinematicViscosity_[elementIdx]
                                            / (ParentType::betaOmega() * pow(wallDistance, 2));
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet values at the boundary.
     *
     * \param element The finite element
     * \param scvf the sub control volume face
     * \note used for cell-centered discretization schemes
     */
    PrimaryVariables dirichlet(const Element &element, const SubControlVolumeFace &scvf) const
    {
        PrimaryVariables values(0.0);
        const auto pos = scvf.ipGlobal();
        values = initialAtPos(pos);
        if (!onLowerBoundary_(pos))
        {
            values[Indices::conti0EqIdx + 1] = refMoleFrac();
            values[Indices::velocityXIdx] = refVelocity();
            values[Indices::temperatureIdx] = refTemperature();
        }

        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a Neumann control volume.
     *
     * \param element The element for which the Neumann boundary condition is set
     * \param fvGeomentry The fvGeometry
     * \param elemVolVars The element volume variables
     * \param elemFaceVars The element face variables
     * \param scvf The boundary sub control volume face
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElementFaceVariables& elemFaceVars,
                        const SubControlVolumeFace& scvf) const
    {
        PrimaryVariables values(0.0);

        if(couplingManager().isCoupledEntity(CouplingManager::stokesIdx, scvf) && time_>=initializationTime_)
        {
            values[Indices::momentumYBalanceIdx] = couplingManager().couplingData().momentumCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);

            const auto massFlux = couplingManager().couplingData().massCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
            values[Indices::conti0EqIdx] = massFlux[0];
            values[Indices::conti0EqIdx + 1] = massFlux[1];

            values[Indices::energyEqIdx] = couplingManager().couplingData().energyCouplingCondition(element, fvGeometry, elemVolVars, elemFaceVars, scvf);
        }
        return values;
    }

    // \}

    /*!
     * \brief Set the coupling manager
     */
    void setCouplingManager(std::shared_ptr<CouplingManager> cm)
    { couplingManager_ = cm; }

    /*!
     * \brief Get the coupling manager
     */
    const CouplingManager& couplingManager() const
    { return *couplingManager_; }

   /*!
     * \name Volume terms
     */
    // \{

   /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        FluidState fluidState;
        fluidState.setPressure(0, 1e5);
        fluidState.setTemperature(273.15+16);
        fluidState.setMoleFraction(0, 0, 1);
        Scalar density = FluidSystem::density(fluidState, 0);

        PrimaryVariables values(0.0);
        values[Indices::pressureIdx] = refPressure() + density*this->gravity()[1]*(globalPos[1] - this->gridGeometry().bBoxMin()[1]);
        values[Indices::conti0EqIdx + 1] = 0.001;
        values[Indices::velocityXIdx] = 0.3;

        values[Indices::temperatureIdx] = 273.15+16;

        values[Indices::turbulentKineticEnergyEqIdx] = turbulentKineticEnergy_;
        values[Indices::dissipationEqIdx] = dissipation_;

        if (isOnWallAtPos(globalPos))
        {
            values[Indices::turbulentKineticEnergyEqIdx] = 0.0;
            values[Indices::dissipationEqIdx] = 0.0;
            values[Indices::velocityXIdx] = 0.0;
        }
        return values;
    }

    //! \brief Returns the reference velocity.
    const Scalar refVelocity() const
    {
        Scalar refVelocity = evaluateData(windSpeedDataPM_, previousTime(), time());
        return refVelocity ;
    }

    //! \brief Returns the reference pressure.
    const Scalar refPressure() const
    { return refPressure_; }

    //! \brief Returns the reference mass fraction.
    const Scalar refMoleFrac() const
    {
        Scalar moleFracH2O =
        evaluateData(relativeHumidityDataPM_, previousTime(), time())/100*H2O::vaporPressure(refTemperature())/1e5;
        return moleFracH2O;
    }

    //! \brief Returns the reference temperature.
    const Scalar refTemperature() const
    {
         Scalar temp = evaluateData(temperatureDataPM_, previousTime(), time());
         return temp;
    }


    void setTimeLoop(TimeLoopPtr timeLoop)
    { timeLoop_ = timeLoop; }

    /*!
     * \brief Returns the intrinsic permeability of required as input parameter
     *        for the Beavers-Joseph-Saffman boundary condition.
     */
    Scalar permeability(const Element& element, const SubControlVolumeFace& scvf) const
    {
        return couplingManager().couplingData().darcyPermeability(element, scvf);
    }

    /*!
     * \brief Returns the alpha value required as input parameter for the Beavers-Joseph-Saffman boundary condition
     */
    Scalar alphaBJ(const SubControlVolumeFace& scvf) const
    {
        return couplingManager().problem(CouplingManager::darcyIdx).spatialParams().beaversJosephCoeffAtPos(scvf.center());
    }


    bool isOnWall(const SubControlVolumeFace& scvf) const
    {
      GlobalPosition globalPos = scvf.ipGlobal();
      return isOnWallAtPos(globalPos);
    }

    bool isOnWallAtPos(const GlobalPosition &globalPos) const
    {
        return ( onLowerBoundary_(globalPos));
    }

    // \}

private:
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->gridGeometry().bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->gridGeometry().bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->gridGeometry().bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->gridGeometry().bBoxMax()[1] - eps_; }

    // the height of the free-flow domain
    const Scalar height_() const
    { return this->gridGeometry().bBoxMax()[1] - this->gridGeometry().bBoxMin()[1]; }

    Scalar eps_;

    Scalar refVelocity_;
    Scalar refPressure_;
    Scalar refMoleFrac_;
    Scalar refTemperature_;
    Scalar turbulentKineticEnergy_;
    Scalar dissipation_;
    Scalar time_=0.0;
    Scalar previousTime_=0.0;
    Scalar initializationTime_;

    TimeLoopPtr timeLoop_;

    std::shared_ptr<CouplingManager> couplingManager_;

    std::string temperatureDataFilePM_;
    std::vector<double> temperatureDataPM_[2];
    std::string relativeHumidityDataFilePM_;
    std::vector<double> relativeHumidityDataPM_[2];

    // for laminar/turbulent flow
    std::string windSpeedDataFilePM_;
    std::vector<double> windSpeedDataPM_[2];
};
} //end namespace

#endif // DUMUX_STOKES1P2C_SUBPROBLEM_HH
