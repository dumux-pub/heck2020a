SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

K. Heck, E. Coltman, J. Schneider, R. Helmig<br>
Influence of radiation on evaporation rates: a numerical analysis


Installation
============

The easiest way to install this module is to create a new directory and clone this module:
```
mkdir heck2020a && cd heck2020a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/heck2020a.git
```

After that, execute the file [installHeck2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Heck2020a/raw/master/installHeck2020a.sh).
```
chmod +x heck2020a/installHeck2020a.sh
./heck2020a/installHeck2020a.sh
```

This should automatically download all necessary modules and check out the correct versions. Afterwards dunecontrol is run. For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installHeck2020a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Heck2020a/raw/master/installHeck2020a.sh).


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir heck2020a
cd heck2020a
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/heck2020a/-/raw/master/docker_heck2020a.sh
```

Open the Docker Container
```bash
bash docker_heck2020a.sh open
```

Applications
============

You can find all examples in the appl folder. you may build all executables by running

```bash
cd heck2020a/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following paths

- appl/example1
- appl/example2
- appl/example2_turbulent
- appl/example3
- appl/example4

They can be executed with an input file e.g., by running

```bash
cd appl/example1
./test_stokes1p2cdarcy2p2c_radiation params_silt.input
```
